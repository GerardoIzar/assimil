- Hay una carpeta por cada idioma:
        1. Alemán
        2. Francés
        3. Inglés Americano
        4. Inglés Británico
        5. Italiano
        6. Japonés
        7. Polaco
        8. Portugués
        9. Ruso
    
- Es necesario entrar a la rama del idioma deseado para posteriormente descargar
    el libro y los audios.